import React, { useState } from "react";
import "./Carousel.css";
import { images } from "../data/CarouselData";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

const Carousel = () => {
  const [index, setIndex] = useState(0);
  const [heading, setHeading] = useState(images[index].title);
  const [subheading, setSubHeading] = useState(images[index].subtitle);

  const handlePrev = () => {
    const newIndex = index === 0 ? 2 : index - 1;
    setIndex(newIndex);
    setHeading(images[newIndex].title);
    setSubHeading(images[newIndex].subtitle);
  };

  const handleNext = () => {
    const newIndex = index === 2 ? 0 : index + 1;
    setIndex(newIndex);
    setHeading(images[newIndex].title);
    setSubHeading(images[newIndex].subtitle);
  };

  const mainStyle = {
    backgroundImage: `url(${images[index].img})`,
  };

  return (
    <div className="something">
      <div className="main" style={mainStyle}>
        <ArrowBackIosIcon className="arrows" onClick={handlePrev} />
        <div className="context">
          <div className="Heading">{heading}</div>
          <div className="subHeading">{subheading}</div>
        </div>
        <ArrowForwardIosIcon className="arrows" onClick={handleNext} />
      </div>
    </div>
  );
};

export default Carousel;
